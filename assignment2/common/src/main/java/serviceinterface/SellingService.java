package serviceinterface;

import entities.Car;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface SellingService extends Remote {

    double computeSellingPrice(Car car) throws RemoteException;
}
