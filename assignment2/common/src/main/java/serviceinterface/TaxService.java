package serviceinterface;

import entities.Car;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface TaxService extends Remote {

    double computeTax(Car car) throws RemoteException;
}
