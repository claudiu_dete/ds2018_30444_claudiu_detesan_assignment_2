import service.ServiceImpl;
import serviceinterface.TaxService;

import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class ServerStart {

    public static void main(String[] args) {
        System.out.println("Server has started");
        try {

            TaxService taxService = new ServiceImpl();
            TaxService stub = (TaxService) UnicastRemoteObject
                    .exportObject(taxService, 0);
            Registry registry = LocateRegistry.createRegistry(1099);
            registry.bind("TaxService", stub);
        }
        catch(RemoteException e) {
            System.out.println("Something went wrong with the connection");
            e.printStackTrace();
        }
        catch(AlreadyBoundException e){
            e.printStackTrace();
        }
    }
}
