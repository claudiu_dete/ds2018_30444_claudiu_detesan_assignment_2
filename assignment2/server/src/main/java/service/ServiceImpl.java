package service;

import entities.Car;
import serviceinterface.SellingService;
import serviceinterface.TaxService;

public class ServiceImpl implements TaxService, SellingService {

    private static final String ENGINE_ERROR = "Engine size must be positive!";
    private static final String PRICE_ERROR = "Price must be positive";
    private static final String YEAR_ERROR = "Year must be in the past";

    public double computeTax(Car car) {
        if(car.getEngineSize()<0) throw new IllegalArgumentException(ENGINE_ERROR);
        int sum = 0;
        if(car.getEngineSize() > 1601) sum = 18;
        if(car.getEngineSize() > 2001) sum = 72;
        if(car.getEngineSize() > 2601) sum = 144;
        if(car.getEngineSize() > 3001) sum = 290;
        return car.getEngineSize() / 200.0 * sum;
    }

    public double computeSellingPrice(Car car){
        if(car.getPrice()<0) throw new IllegalArgumentException(PRICE_ERROR);
        if(car.getYear()>2018 || car.getYear()<0) throw new IllegalArgumentException(YEAR_ERROR);
        return  2018- car.getYear()<7 ? car.getPrice() - (car.getPrice() * (2018 - car.getYear()) / 7) : 0;
    }
}
