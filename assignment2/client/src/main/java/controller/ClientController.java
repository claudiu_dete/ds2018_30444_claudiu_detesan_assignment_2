package controller;

import entities.Car;
import serviceinterface.SellingService;
import serviceinterface.TaxService;
import view.ClientView;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class ClientController {

    private ClientView clientView;
    private TaxService taxService;
    private SellingService sellingService;


    public ClientController() {
        clientView = new ClientView();
        clientView.setVisible(true);

        try {
            Registry registry = LocateRegistry.getRegistry();
            taxService = (TaxService) registry.lookup("TaxService");
            sellingService = (SellingService) registry.lookup("TaxService");
            clientView.addBtnTaxActionListener(new TaxActionListener());
            clientView.addBtnPriceActionListener(new SellingPriceActionListener());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void displayErrorMessage(String message) {
        JOptionPane.showMessageDialog(clientView, message, "Error", JOptionPane.ERROR_MESSAGE);
    }


    class TaxActionListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            try {
                int year = Integer.parseInt(clientView.getYear());
                int engineSize = Integer.parseInt(clientView.getEngineSize());
                double price = Double.parseDouble(clientView.getPrice());

                Car car = new Car(year, engineSize, price);
                double tax = taxService.computeTax(car);
                clientView.printResult("The tax for this car is:\n" + tax);

            } catch (NumberFormatException er) {
                displayErrorMessage("All fields must be numbers!");
            } catch (RemoteException er) {
                displayErrorMessage("Error in connection");
            } catch (IllegalArgumentException er) {
                displayErrorMessage(er.getMessage());
            }
        }
    }

    class SellingPriceActionListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            try {
                int year = Integer.parseInt(clientView.getYear());
                int engineSize = Integer.parseInt(clientView.getEngineSize());
                double price = Double.parseDouble(clientView.getPrice());
                Car car = new Car(year, engineSize, price);
                double sellingPrice = sellingService.computeSellingPrice(car);
                clientView.printResult("The selling price for this car is:\n" + sellingPrice);

            } catch (NumberFormatException er) {
                displayErrorMessage("All fields must be numbers!");
            } catch (RemoteException er) {
                displayErrorMessage("Error in connection");
            } catch (IllegalArgumentException er) {
                displayErrorMessage(er.getMessage());
            }
        }
    }


}
