package view;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.event.ActionListener;

public class ClientView extends JFrame {

    private JPanel contentPane;
    private JTextField prodYearText;
    private JTextField engineSizeText;
    private JTextField priceText;
    private JButton btnTax;
    private JButton btnPrice;
    private JTextArea textArea;

    public ClientView() {
        setTitle("Java RMI");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(300, 100, 450, 300);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        JLabel lblInsertCar = new JLabel("Introduce the car information:");
        lblInsertCar.setBounds(10, 11, 200, 14);
        contentPane.add(lblInsertCar);

        JLabel lblProdYear = new JLabel("Production year:");
        lblProdYear.setBounds(10, 36, 100, 14);
        contentPane.add(lblProdYear);

        JLabel lblEngineSize = new JLabel("Engine size:");
        lblEngineSize.setBounds(10, 61, 100, 14);
        contentPane.add(lblEngineSize);

        JLabel lblPurchasingPrice = new JLabel("Purchasing price:");
        lblPurchasingPrice.setBounds(10, 86, 100, 14);
        contentPane.add(lblPurchasingPrice);

        prodYearText = new JTextField();
        prodYearText.setBounds(120, 33, 86, 20);
        contentPane.add(prodYearText);
        prodYearText.setColumns(10);

        engineSizeText = new JTextField();
        engineSizeText.setBounds(120, 58, 86, 20);
        contentPane.add(engineSizeText);
        engineSizeText.setColumns(10);

        priceText = new JTextField();
        priceText.setBounds(120, 83, 86, 20);
        contentPane.add(priceText);
        priceText.setColumns(10);

        btnTax = new JButton("Compute Tax");
        btnTax.setBounds(10, 132, 120, 23);
        contentPane.add(btnTax);

        btnPrice = new JButton("Selling price");
        btnPrice.setBounds(10, 165, 120, 23);
        contentPane.add(btnPrice);

        textArea = new JTextArea();
        textArea.setBounds(235, 131, 171, 70);
        contentPane.add(textArea);
    }

    public void addBtnPriceActionListener(ActionListener e) {
        btnPrice.addActionListener(e);
    }

    public void addBtnTaxActionListener(ActionListener e) {
        btnTax.addActionListener(e);
    }

    public String getYear() {
        return prodYearText.getText();
    }

    public String getEngineSize() {
        return engineSizeText.getText();
    }

    public String getPrice() {
        return priceText.getText();
    }

    public void printResult(String message){
        textArea.setText(message);
    }



}
